package com.example.attendancee;

public class StudentBean {
    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getSub_dept() {
        return sub_dept;
    }

    public void setSub_dept(String sub_dept) {
        this.sub_dept = sub_dept;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRollnumber() {
        return rollnumber;
    }

    public void setRollnumber(int rollnumber) {
        this.rollnumber = rollnumber;
    }

    public StudentBean(String sem, String sub_name, String sub_dept, String sub_code, String type, int rollnumber) {
        this.sem = sem;
        this.sub_name = sub_name;
        this.sub_dept = sub_dept;
        this.sub_code = sub_code;
        this.type = type;
        this.rollnumber = rollnumber;
    }

    String sem, sub_name, sub_dept, sub_code, type;
    int rollnumber;

}
