package com.example.attendancee;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.Tag;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class register extends AppCompatActivity implements View.OnClickListener {
    EditText editname, editrmail, editphone, editrpass, editrtdept;
    Button buttonlogin, buttonregister;
    Button teachbackr;
    FirebaseAuth auth;
    CircleImageView addphoto;
    private Uri capImageURI;
    ProgressDialog progressDialog;
    private static final int GALLERY_REQUEST = 1;
    private static final int CAMERA_REQUEST = 2;
    private StorageReference storageReference;

    // Access a Cloud Firestore instance from your Activity
    FirebaseFirestore db;
    String Tag = "o";

//    FirebaseDatabase database;
//    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editname = findViewById(R.id.editname);
        editrmail = findViewById(R.id.editrmail);
        editphone = findViewById(R.id.editphone);
        editrpass = findViewById(R.id.editrpass);
        buttonlogin = findViewById(R.id.buttonlogin);
        buttonregister = findViewById(R.id.buttonregister);
        editrtdept = findViewById(R.id.editrdept);
        addphoto = findViewById(R.id.teachimg);
        teachbackr = findViewById(R.id.teachbackr);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a second ...");
        addphoto.setOnClickListener(this);

        storageReference = FirebaseStorage.getInstance().getReference().child("Database").child("Users");

        teachbackr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
//        database=FirebaseDatabase.getInstance();
//        databaseReference=database.getReference("User");
        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(register.this, MainActivity.class));
            }
        });
        buttonregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doregister();
            }
        });
 progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a second ...");
    }

    private void doregister() {
        final String name = editname.getText().toString().trim();
        final String mail = editrmail.getText().toString().trim();
        final String phone = editphone.getText().toString().trim();
        final String pass = editrpass.getText().toString().trim();
        final String deptt = editrtdept.getText().toString().trim();
        if (name.isEmpty()) {
            editname.setError("Enter Student name");
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            editrmail.setError("Enter valid mail");
            return;
        }
        if (phone.isEmpty() || phone.length() < 10) {
            editphone.setError("Enter Phone");
            return;
        }
        if (pass.isEmpty() || pass.length() < 6) {
            editrpass.setError("Enter password");
            return;

        }
        if (deptt.isEmpty()) {
            editrtdept.setError("Enter Dept name");
            return;
        }
        if (capImageURI == null) {
            Toast.makeText(this, "Please select a profile image to continue", Toast.LENGTH_SHORT).show();
            return;
        }
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), capImageURI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, outputStream);
        final byte[] fileInBytes = outputStream.toByteArray();


        auth.createUserWithEmailAndPassword(mail, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final StorageReference filePath = storageReference.
                            child(auth.getCurrentUser().getUid())
                            .child("ProfilePic").child(capImageURI.getLastPathSegment());
                    filePath.putBytes(fileInBytes).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    storedata(name, mail, phone, pass, deptt, uri);
                                    Toast.makeText(register.this, "Registered", Toast.LENGTH_SHORT).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(register.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                } else {
                    Toast.makeText(register.this, "User not Registered", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(register.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void storedata(String name, String mail, String phone, String password, String deptt, Uri uri) {
        User teachdetails = new User(name, mail, phone, password, deptt, "Teacher", String.valueOf(uri));
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//        databaseReference.child(uid).setValue(user);
        db.collection("users").document(uid).set(teachdetails).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(Tag, "Teacher added with id");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(Tag, "Error adding", e);
            }
        });


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.buttonregister:
                doregister();
                break;
            case R.id.teachbackr:
                Home();
                break;
            case R.id.teachimg:
                iphoto();
                break;
        }

    }

    private void iphoto() {
        final CharSequence[] items = {"Take a new photo", "Choose from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(register.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (items[i].equals("Take a new photo")) {
                    //request permission start camera intent
                    requestCameraPermission();
                } else if (items[i].equals("Choose from gallery")) {
                    //request gallery permission and start gallery intent
                    requestGalleryPermission();
                } else if (items[i].equals("Cancel")) {
                    //dismiss the alert dialog
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void requestGalleryPermission() {
        int result = ContextCompat
                .checkSelfPermission(register.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            openGallery();
        } else {
            ActivityCompat
                    .requestPermissions(register.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            GALLERY_REQUEST);
        }
    }

    //Camera
    private void requestCameraPermission() {
        int result = ContextCompat.checkSelfPermission(register.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            startCamera();
        } else {
            ActivityCompat.requestPermissions(register.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                } else {
                    Toast.makeText(this, "Permission denied to open Camera", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case GALLERY_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                } else {
                    Toast.makeText(this, "Permission denied to open Gallery", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void openGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, GALLERY_REQUEST);
    }

    private void startCamera() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent takepicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takepicture.resolveActivity(getPackageManager()) != null) {
            String filename = "temp.jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, filename);
            capImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            takepicture.putExtra(MediaStore.EXTRA_OUTPUT, capImageURI);
            startActivityForResult(takepicture, CAMERA_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_REQUEST: {
                if (resultCode == RESULT_OK && data.getData() != null) {
                    capImageURI = data.getData();
                    setProfileImage(data.getData());
                } else {
                    Toast.makeText(this, "No Image Selected! Try AgainNo Image Selected! Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case CAMERA_REQUEST: {
                if (resultCode == RESULT_OK) {
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = managedQuery(capImageURI, projection, null, null, null);
                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String picturepath = cursor.getString(column_index_data);
                    capImageURI = Uri.parse("file://" + picturepath);
                    setProfileImage(capImageURI);
                } else {
                    Toast.makeText(this, "No Image Captured! Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void setProfileImage(Uri data) {
        addphoto.setImageURI(data);
    }


    private void Home() {
        Intent intent = new Intent(this, Hom_activity.class);
        startActivity(intent);
        finish();
    }
}

