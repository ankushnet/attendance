package com.example.attendancee;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class UpdateAdapter extends RecyclerView.Adapter<UpdateAdapter.Myholdd> {
    FirebaseAuth auth = FirebaseAuth.getInstance();
    List<Student> list;
    Context context;

    public UpdateAdapter(List<Student> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public UpdateAdapter.Myholdd onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.update_stu, viewGroup, false);
        return new Myholdd(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UpdateAdapter.Myholdd myholdd, final int i) {
        Student student = list.get(i);
        myholdd.txtrollup.setText(String.valueOf(student.roll_no));
        myholdd.editnameup.setText(student.name);
        myholdd.editnameup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                list.get(i).setName(myholdd.editnameup.getText().toString());
//                Toast.makeText(context, myholdd.editnameup.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Myholdd extends RecyclerView.ViewHolder {
        Button update;
        TextView txtrollup;
        EditText editnameup;

        public Myholdd(@NonNull View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            txtrollup = itemView.findViewById(R.id.txtrollupdate);
            editnameup = itemView.findViewById(R.id.editnameupdate);
            update = itemView.findViewById(R.id.btnupdate);

        }
    }
}
