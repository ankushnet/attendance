package com.example.attendancee;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class
Add_sub extends AppCompatActivity {
    int m_int = 1;
    Button btn_date;
    FloatingActionButton btn_add;
    Button update_data;
    TextView sem_number;
    TextView date;
    EditText sub_name, sub_code, sub_dept;
    RadioGroup radioGroup;
    RadioButton lecture, lab;
    String type = "";
    FirebaseAuth auth;
    FirebaseFirestore db;
    ProgressDialog progressDialog;
    String tag = "One";
    EditText rollfrom, rollto;
    String id;
    List<Subjects> listt;
    final Long timestemp = new Date().getTime();

//    Spinner spin_schedule;
//    String[] days = {"Monday"rollfrom, "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardview);
        auth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        date = findViewById(R.id.datesched);
        date.setText(getDate(timestemp));
        progressDialog.setMessage("wait a sec...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        id = getIntent().getStringExtra("id");
        db = FirebaseFirestore.getInstance();
        listt = new ArrayList<>();

        sem_number = findViewById(R.id.sem_number);
        sub_name = findViewById(R.id.sub_name);

        sub_code = findViewById(R.id.sub_code);
        sub_dept = findViewById(R.id.sub_dept);
        rollfrom = findViewById(R.id.roll_from);
        rollto = findViewById(R.id.roll_to);
        radioGroup = findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbtn_lecture) {
                    type = lecture.getText().toString().trim();

                }
                if (checkedId == R.id.rbtn_lab) {
                    type = lab.getText().toString().trim();
                }

            }
        });
        lecture = findViewById(R.id.rbtn_lecture);
        lab = findViewById(R.id.rbtn_lab);
        btn_add = findViewById(R.id.button_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (sub_name.getText().toString().isEmpty()) {
                    sub_name.setError("Enter subject name");

                } else if (sub_code.getText().toString().isEmpty()) {
                    sub_code.setError("Enter subjectCode ");
                    return;
                } else if (sub_dept.getText().toString().isEmpty()) {
                    sub_dept.setError("Enter department");
                    return;
                } else if (rollfrom.getText().toString().isEmpty()) {
                    rollfrom.setError("Enter a number");
                    return;
                } else if (rollto.getText().toString().isEmpty()) {
                    rollto.setError("Enter a number");
                    return;
                } else {
                    String semnumber = sem_number.getText().toString().trim();
                    String subname = sub_name.getText().toString().trim();
                    String subcode = sub_code.getText().toString().trim();
                    String subdept = sub_dept.getText().toString().trim();
                    String datee = date.getText().toString().trim();
                    int roll_from = Integer.parseInt(rollfrom.getText().toString().trim());
                    int roll_to = Integer.parseInt(rollto.getText().toString().trim());
//                    Scanner in = new Scanner(System.in).useDelimiter("[,\\s+]");
//                 String   input = rollskip.getText().toString();
//                    String arraayy[] = input.split(" ");


//                    Log.d("array", "onClick: " + arraayy.length);
//                    int roll_skip = Integer.parseInt(rollskip.getText().toString());

                    if (roll_from > roll_to) {
                        rollfrom.setError("Starting Number should be greater");
                        return;
                    }
                    Subjects subjects = null;
                    if (id == null) {
                        List<Student> student = new ArrayList<>();
                        for (int c = roll_from; c <= roll_to; c++) {
//                            if (c == Integer.parseInt(arraayy[c]))
//                                continue;
                            Student stu = new Student("Student " + c, c, false, datee);
                            student.add(stu);
                        }
                        subjects = new Subjects(semnumber, subname, subdept, subcode, type, student, roll_from, roll_to, datee);
                        storedata(subjects);
                    } else {
                        List<Student> student = new ArrayList<>();
                        for (int c = roll_from; c <= roll_to; c++) {
//                            if (c == Integer.parseInt(arraayy[c]))
//                                continue;
                            Student stu = new Student("Student " + c, c, false, datee);

                            student.add(stu);

                        }

                        subjects = new Subjects(semnumber, subname, subdept, subcode, type, student, roll_from, roll_to, datee);
                        updatedata(id, subjects);
                    }
                }

            }
        });
//        btn_date = findViewById(R.id.btn_date);
//        btn_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DialogFragment newFragment = new TimePickerFragment();
//                newFragment.show(getSupportFragmentManager(), "timePicker");
//                spin_schedule = findViewById(R.id.spin_schedule);
//                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, days);
//                arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//                spin_schedule.setAdapter(arrayAdapter);
//                spin_schedule.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        Toast.makeText(Add_sub.this, days[position], Toast.LENGTH_SHORT).show();
//
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//            }
//        });
        Intent intent = getIntent();
        if (intent.getParcelableExtra("subjects") != null) {

            Subjects subjects = intent.getParcelableExtra("subjects");
            String subcodee = subjects.getSub_code();
            String subnamee = subjects.getSub_name();
            String dept = subjects.getSub_dept();
            int rollfromm = subjects.getRollfrom();
            int rolltoo = subjects.getRollto();
            String type = subjects.getType();
            sub_name.setText(subnamee);
            sub_code.setText(subcodee);
            sub_dept.setText(dept);
            rollfrom.setText(String.valueOf(rollfromm));
            rollto.setText(String.valueOf(rolltoo));

            if (type.equals("Lecture")) {
                lecture.setChecked(true);
            } else if (type.equals("Lab")) {
                lab.setChecked(true);
            }

        } else {

            Log.e("error", "error");
        }

    }

    private void updatedata(String id, Subjects subjects) {
        db.collection("Subjects").document(FirebaseAuth.getInstance().getUid()).
                collection("subjects").
                document(id).set(subjects)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Add_sub.this, "Data updated", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Add_sub.this, HomePage.class);
                            startActivity(intent);

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Add_sub.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getdata() {

        String uid = auth.getCurrentUser().getUid();
        db.collection("Subjects").document(uid).collection("subjects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Subjects subjects = document.toObject(Subjects.class);
                                Log.e("subjectCode ", subjects.sub_name);
                            }
                        } else {
                            Log.e(tag, "Error getting documents.", task.getException());
                        }
                    }
                });

    }

    private void storedata(Subjects subjects) {
        progressDialog.show();

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db.collection("Subjects").document(uid).collection("subjects").document().set(subjects).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                util.toast(Add_sub.this, "Subject added");
                startActivity(new Intent(Add_sub.this, HomePage.class));

            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.hide();
                Log.w(tag, "Error");
            }
        }).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    progressDialog.dismiss();
            }
        });

    }

    public void DecreaseInt(View view) {

        if (m_int > 1)
            m_int = m_int - 1;
        else {
            System.out.print("Semester starts from one");
        }
        display(m_int);

    }

    public void IncreaseInt(View view) {
        if (m_int < 9) {
            m_int = m_int + 1;
        } else {
            m_int = m_int - 1;
        }

        display(m_int);
    }

    private void display(int number) {
        TextView textView = findViewById(R.id.sem_number);
        textView.setText("" + number);


    }

    private String getDate(long time_stamp_server) {
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy");
        return formatter.format(time_stamp_server);
    }

}


