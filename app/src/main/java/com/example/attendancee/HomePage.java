package com.example.attendancee;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomePage extends AppCompatActivity implements StudentNamesAdapter.doAlert, View.OnClickListener {
    RecyclerView recyclerView;
    StudentNamesAdapter adapter;
    FirebaseFirestore db;
    Button btn_log;
    TextView status;
    TextView tvname, tvemail, tvtitle;
    ImageView btnnav;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ProgressDialog progressDialog;
    CircleImageView teachimgnav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homenav);
        drawerLayout = findViewById(R.id.drawer_layout);
        progressDialog = new ProgressDialog(this);
        teachimgnav = findViewById(R.id.teachimgnav);
        navigationView = findViewById(R.id.nav_view);
        status = findViewById(R.id.status);

        progressDialog.setMessage("wait a sec...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        btnnav = findViewById(R.id.btnnav);

        // listView = findViewById(R.id.listview);
        //  list = new ArrayList<>();
        //adapter = new ArrayAdapter<String>(this, R.layout.details, R.id.txt, list);
        btnnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.START))
                    drawerLayout.closeDrawer(Gravity.START);
                else {
                    drawerLayout.openDrawer(Gravity.START);
                }


            }
        });
        setname();
        View view = navigationView.getHeaderView(0);
        tvname = view.findViewById(R.id.tvnamed);
        teachimgnav = view.findViewById(R.id.teachimgnav);
        LinearLayout home = view.findViewById(R.id.header_home);
        LinearLayout status = view.findViewById(R.id.header_status_teacher);
        LinearLayout changeassword = view.findViewById(R.id.header_changepass);
        LinearLayout logout = view.findViewById(R.id.header_logout);
        LinearLayout profile = view.findViewById(R.id.header_profile_teach);

        home.setOnClickListener(this);
        status.setOnClickListener(this);
        changeassword.setOnClickListener(this);
        logout.setOnClickListener(this);
        profile.setOnClickListener(this);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                }
                return true;
            }
        });


        db = FirebaseFirestore.getInstance();
        recyclerView = findViewById(R.id.recycler);
        adapter = new StudentNamesAdapter(getApplicationContext(), this, "");
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        getdata();

        findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomePage.this, Add_sub.class));
            }
        });

    }

    private void setname() {

        db = FirebaseFirestore.getInstance();
        db.collection("users").document(FirebaseAuth.getInstance().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {

                    User user = task.getResult().toObject(User.class);
                    tvname.setText(user.name);
                    Picasso.get().load(user.getProfileuri()).into(teachimgnav);


                }
            }
        });
    }

    private void myprofile() {


    }

    private void getdata() {
        db.collection("Subjects").document(FirebaseAuth.getInstance().getUid()).collection("subjects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().size() == 0) {
                                status.setVisibility(View.VISIBLE);
                            } else {
                                status.setVisibility(View.GONE);


                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Subjects subjects = document.toObject(Subjects.class);
                                    adapter.addData(subjects, document.getId());
                                    adapter.notifyDataSetChanged();
                                    Log.e("subjectCode ", subjects.sub_name);
                                }
                            }
                        }
                    }
                });

    }

    @Override
    public void onAlert(final int i, final String s) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure, You wanted to make decision");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        String uid = FirebaseAuth.getInstance().getUid();
                        DocumentReference reference = db.collection("Subjects").document(uid).collection("subjects").document(s);
                        reference.delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        adapter.removeData(i);
                                        adapter.notifyDataSetChanged();
                                        deleteAttendence(s);

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(HomePage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteAttendence(String s) {
        db.collection("Subjects")
                .document(FirebaseAuth.getInstance().getUid())
                .collection("attendence").document(s).delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(HomePage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    Toast.makeText(HomePage.this, "Subject Deleted", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_home: {
                startActivity(new Intent(HomePage.this, HomePage.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;
            }
            case R.id.header_status_teacher: {
                Intent intent = new Intent(getApplicationContext(), activity_viewstatus.class);
                intent.putExtra("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());
                startActivity(intent);
                drawerLayout.closeDrawer(Gravity.START);
                break;
            }
            case R.id.header_changepass: {
                startActivity(new Intent(getApplicationContext(), changepass.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;
            }
            case R.id.header_logout: {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(HomePage.this, MainActivity.class));
                finish();
                drawerLayout.closeDrawer(Gravity.START);
                break;
            }
            case R.id.header_profile_teach: {
                startActivity(new Intent(HomePage.this, ProfileActivity.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;
            }
        }


    }
}
