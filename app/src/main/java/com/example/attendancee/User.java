package com.example.attendancee;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

class User {
    User() {

    }

    public User(String name, String mail, String phone, String password, String dept, String type, String profileuri) {
        this.name = name;
        this.mail = mail;
        this.phone = phone;
        this.password = password;
        this.timestemp = new Date().getTime();
        this.dept = dept;
        this.type = type;
        this.uid = FirebaseAuth.getInstance().getUid();
        this.profileuri = profileuri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    String name;
    String mail;
    String phone;
    String password;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private String uid;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    String dept;

    public Long getTimestemp() {
        return timestemp;
    }

    public void setTimestemp(Long timestemp) {
        this.timestemp = timestemp;
    }

    Long timestemp = 0L;

    public String getProfileuri() {
        return profileuri;
    }

    public void setProfileuri(String profileuri) {
        this.profileuri = profileuri;
    }

    String profileuri;

}
