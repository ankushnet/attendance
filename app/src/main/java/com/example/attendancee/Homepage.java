package com.example.attendancee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Homepage extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    ImageView btnnav;
    FloatingActionButton btn_add;
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    User user;
    String uid;
    DrawerLayout drawerLayout;
    NavigationView navigationView;


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START))
            drawerLayout.closeDrawer(Gravity.START);
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        auth = FirebaseAuth.getInstance();
        btn_add = findViewById(R.id.button_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Homepage.this, Add_sub.class));
            }
        });
       /*  database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("User");
        FirebaseUser userr = FirebaseAuth.getInstance().getCurrentUser();
        uid = userr.getUid();
        */
        btnnav = findViewById(R.id.btnnav);
        // listView = findViewById(R.id.listview);
        //  list = new ArrayList<>();
        //adapter = new ArrayAdapter<String>(this, R.layout.details, R.id.txt, list);
        btnnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.START))
                    drawerLayout.closeDrawer(Gravity.START);
                else {
                    drawerLayout.openDrawer(Gravity.START);
                }


            }
        });

/*
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child(uid).child("name").getValue(String.class);
                String mail = dataSnapshot.child(uid).child("mail").getValue(String.class);

                String password = dataSnapshot.child(uid).child("password").getValue(String.class);
                String phone = dataSnapshot.child(uid).child("phone").getValue(String.class);


                // user = ds.getValue(User.class);
                list.add(name);
                list.add(mail);
                list.add(phone);
                list.add(password);



                listView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Homepage.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        */
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(Homepage.this, Homepage.class));
                        break;
                    case R.id.change_pass:
                        startActivity(new Intent(Homepage.this, changepass.class));
                        break;
                    case R.id.log_out:
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(Homepage.this, MainActivity.class));
                        break;
//                    case R.id.add_subject:
//                        startActivity(new Intent(Homepage.this, Add_subject.class));
//                        break;
//                    case R.id.enter_student:
//                        startActivity(new Intent(Homepage.this, SubjectList.class));
//                        break;
//                    case R.id.student_details:
//                        startActivity(new Intent(Homepage.this, SelectRollNo.class));
//                        break;


                }
                return true;
            }
        });


    }
}
