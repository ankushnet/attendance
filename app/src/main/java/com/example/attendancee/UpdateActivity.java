package com.example.attendancee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

public class UpdateActivity extends AppCompatActivity {
    RecyclerView recyclerView_u;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    UpdateAdapter adapter;
    String id;
    FloatingActionButton btnupdate;
    Button backupdate;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updatenames);
        btnupdate = findViewById(R.id.btnupdate);
        backupdate = findViewById(R.id.btnbacko);
        backupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HomePage.class));
            }
        });
        auth = FirebaseAuth.getInstance();
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRealtimeUpdate();

            }
        });
        Subjects subject = new Gson().fromJson(getIntent().getStringExtra("list"), Subjects.class);
        id = getIntent().getStringExtra("id");
        recyclerView_u = findViewById(R.id.update_recycle);

        adapter = new UpdateAdapter(subject.studentList, this);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView_u.setLayoutManager(manager);
        recyclerView_u.setAdapter(adapter);
    }


    private void addRealtimeUpdate() {
        db.collection("Subjects").document(FirebaseAuth.getInstance().getUid()).
                collection("subjects").
                document(id).update("studentList", adapter.list)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UpdateActivity.this, "Data updated", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdateActivity.this, HomePage.class);
                            startActivity(intent);

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UpdateActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getdata() {

        String uid = auth.getCurrentUser().getUid();
        db.collection("Subjects").document(uid).collection("subjects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Subjects subjects = document.toObject(Subjects.class);
                                Log.e("subjectCode ", subjects.sub_name);
                            }
                        } else {
                            Log.e("Error", "Error getting documents.", task.getException());
                        }
                    }
                });

    }
//   void update()
//    {
//        HashMap<String,Attendence> map=new HashMap<>();
//        Attendence attendence=new Attendence();
//        attendence.setList(adapter.list);
//        map.put("studentList",attendence)
//        db.collection("Subjects").document(id).update(map)
//    }
}
