package com.example.attendancee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Take_attendance extends AppCompatActivity {
    RecyclerView recyclerView;
    TextView date;
    FloatingActionButton btnattend;
    FirebaseAuth auth;
    FirebaseFirestore db;
    AttenAdapter adapter;
    ImageView btnselectall,btnunselectall;
    Button btnback;
    final Long timestemp = new Date().getTime();


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.takeattend);
        final Subjects subject = new Gson().fromJson(getIntent().getStringExtra("list"), Subjects.class);
        final String subNodeId = getIntent().getStringExtra("id");
        recyclerView = findViewById(R.id.atten_recycler);
        btnselectall = findViewById(R.id.checkall);
        btnunselectall=findViewById(R.id.uncheckall);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        date = findViewById(R.id.txtdate);
        date.setText(getDate(timestemp));
        btnback = findViewById(R.id.btn_backk);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HomePage.class));
            }
        });
        btnattend = findViewById(R.id.btn_attend);
        final AttenAdapter attenadapter = new AttenAdapter(this, subject.studentList, btnselectall,btnunselectall);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(attenadapter);
        btnattend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Student> list = attenadapter.list;
                Attendence attendence = new Attendence();
                attendence.setList(list);
                attendence.setTimestemp(new Date().getTime());
                attendence.setDate(getDate(timestemp));
                attendence.setSubjectCode(subject.sub_code);
                db.collection("Subjects")
                        .document(FirebaseAuth.getInstance().getUid())
                        .collection("attendence").document(subNodeId).collection("attendence").document(date.getText().toString() + "-" + subject.sub_code)
                        .set(attendence).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Take_attendance.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            Toast.makeText(Take_attendance.this, "attendence updated", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }


    private String getDate(long time_stamp_server) {
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy");
        return formatter.format(time_stamp_server);
    }


}
