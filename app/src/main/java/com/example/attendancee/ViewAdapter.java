package com.example.attendancee;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.Myholdu> {
    List<Student> listt = new ArrayList<>();
    Context context;

    public ViewAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public ViewAdapter.Myholdu onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.viewao, viewGroup, false);
        return new Myholdu(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewAdapter.Myholdu myholdu, int i) {
        Student student = listt.get(i);
//        myhold.checkBox.setVisibility(student.showCheckbox ? View.VISIBLE : View.GONE);

        myholdu.rollno.setText(String.valueOf(student.roll_no));
        myholdu.namee.setText(String.valueOf(student.name));
//        myholdu.datee.setText(String.valueOf(student.status));
        if (student.status == true) {
            myholdu.datee.setText("Present");
        } else {
            myholdu.datee.setText(("Absent"));
            myholdu.datee.setBackgroundColor(context.getResources().getColor(R.color.colorRed));

        }


//        myholdu.datee.setText(student.date);


    }

    @Override
    public int getItemCount() {
        return listt.size();
    }

    public void add(List<Student> list) {
        this.listt = list;
        notifyDataSetChanged();
    }

    public class Myholdu extends RecyclerView.ViewHolder {
        TextView namee, rollno;
        TextView datee;

        public Myholdu(@NonNull View itemView) {
            super(itemView);
            namee = itemView.findViewById(R.id.txtnameview);
            rollno = itemView.findViewById(R.id.txtrollview);
            datee = itemView.findViewById(R.id.dateview);
        }
    }
}
