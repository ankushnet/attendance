package com.example.attendancee;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

public class Studentnavigate extends AppCompatActivity implements View.OnClickListener {
    ImageView btnnav;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    FirebaseAuth auth;
    FirebaseFirestore db;
    RecyclerView recyclerView;
    ViewStuAdapter viewStuAdapter;
    TextView tvnamee, tvemaile, tvtitlestu;
    ImageView stuimgnav;

    private static Boolean isStudent = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentnavigate);
        drawerLayout = findViewById(R.id.drawer_layout2);
        recyclerView = findViewById(R.id.recyclerstu);
        viewStuAdapter = new ViewStuAdapter(this);
        navigationView = findViewById(R.id.nav_view2);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        View header = navigationView.getHeaderView(0);
        tvnamee = header.findViewById(R.id.tvnamee);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(viewStuAdapter);
        getsaveddata();
        btnnav = findViewById(R.id.btnnav2);
        btnnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.START))
                    drawerLayout.closeDrawer(Gravity.START);
                else {
                    drawerLayout.openDrawer(Gravity.START);
                }
            }
        });
        setstuname();
        View view = navigationView.getHeaderView(0);
        LinearLayout home = view.findViewById(R.id.header_home_stu);
        LinearLayout status = view.findViewById(R.id.header_status_stu);
        LinearLayout changeassword = view.findViewById(R.id.header_changepass_stu);
        LinearLayout logout = view.findViewById(R.id.header_logout_stu);
        LinearLayout profile = view.findViewById(R.id.header_profile_stu);
        stuimgnav = view.findViewById(R.id.stuimgnav);
        home.setOnClickListener(this);
        status.setOnClickListener(this);
        changeassword.setOnClickListener(this);
        logout.setOnClickListener(this);
        profile.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                }
                return true;
            }

        });

    }

    private void setstuname() {

        db = FirebaseFirestore.getInstance();
        db.collection("users").document(FirebaseAuth.getInstance().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {

                    User user = task.getResult().toObject(User.class);
                    tvnamee.setText(user.name);
                    Picasso.get().load(user.getProfileuri()).into(stuimgnav);


                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_home_stu:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(Studentnavigate.this, Studentnavigate.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.header_profile_stu:
                startActivity(new Intent(Studentnavigate.this, ProfileActivity.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;

            case R.id.header_changepass_stu:
                startActivity(new Intent(Studentnavigate.this, changepass.class));
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.header_logout_stu:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(Studentnavigate.this, StudentLogin.class));
                finish();
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.header_status_stu:
                getstatus();
                drawerLayout.closeDrawer(Gravity.START);
                break;
        }
    }


    private void getsaveddata() {

        db.collection("users").document(auth.getUid())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    User da = task.getResult().toObject(User.class);
                    db.collection("users")
                            .whereEqualTo("dept", da.getDept())
                            .whereEqualTo("type", "Teacher")
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    User teacherstatus = document.toObject(User.class);
                                    viewStuAdapter.addData(teacherstatus, document.getId());
                                    viewStuAdapter.notifyDataSetChanged();
                                    //Log.e("subject ", subjects.sub_name);

                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(Studentnavigate.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void getstatus() {

        db.collection("users").document(auth.getUid())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    User da = task.getResult().toObject(User.class);
                    db.collection("users")
                            .whereEqualTo("dept", da.getDept())
                            .whereEqualTo("type", "Teacher")
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    User teacherstatus = document.toObject(User.class);
                                    Intent intent = new Intent(getApplicationContext(), activity_viewstatus.class);
                                    intent.putExtra("uid", teacherstatus.getUid());
                                    intent.putExtra("isStudent", "true");
                                    startActivity(intent);
                                    //Log.e("subject ", subjects.sub_name);

                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
