package com.example.attendancee;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

class StudentNamesAdapter extends RecyclerView.Adapter<StudentNamesAdapter.Myholder> {
    ArrayList<Subjects> sub_list = new ArrayList();
    ArrayList<String> ids = new ArrayList();
    Context context;
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    doAlert listner;
    String teacherUid;


    public StudentNamesAdapter(Context context, doAlert listner, String uid) {
        this.context = context;
        this.listner = listner;
        teacherUid = uid;


    }

    @NonNull
    @Override
    public StudentNamesAdapter.Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sub, viewGroup, false);
        return new Myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentNamesAdapter.Myholder myholder, final int i) {

        final Subjects subjects = sub_list.get(i);
        myholder.tvsub_name.setText(subjects.sub_name);
        myholder.tvsub_type.setText(subjects.type);
        myholder.tvsem.setText(subjects.sem);
        myholder.tv_dept.setText(subjects.sub_dept);
        myholder.tv_code.setText(subjects.sub_code);
        db.collection("users").document(auth.getUid())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    User da = task.getResult().toObject(User.class);
                    if (da.getType().equals("Teacher")) {
                        myholder.check.setVisibility(View.VISIBLE);
                        myholder.chart.setVisibility(View.VISIBLE);
                        myholder.edit.setVisibility(View.VISIBLE);
                        myholder.person.setVisibility(View.VISIBLE);
                        myholder.delete.setVisibility(View.VISIBLE);

                    } else {

                        myholder.check.setVisibility(View.GONE);
                        myholder.chart.setVisibility(View.VISIBLE);
                        myholder.edit.setVisibility(View.GONE);
                        myholder.person.setVisibility(View.GONE);
                        myholder.delete.setVisibility(View.GONE);


                    }
                }
            }
        });

        if (i % 2 == 1) {
            myholder.cardView.setBackgroundColor(Color.parseColor("#FF0000"));
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            myholder.cardView.setBackgroundColor(Color.parseColor("#FFA500"));
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
        }
        myholder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = new Gson().toJson(subjects);
                Intent intent = new Intent(context, Take_attendance.class);
                intent.putExtra("list", subject);
                intent.putExtra("id", ids.get(i));
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });
        myholder.person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = new Gson().toJson(subjects);
                Intent intent = new Intent(context, UpdateActivity.class);
                intent.putExtra("list", subject);
                intent.putExtra("id", ids.get(i));
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        myholder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.onAlert(i, ids.get(i));
            }
        });
        myholder.chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = new Gson().toJson(subjects);
                Intent intent = new Intent(context, ViewAttendence.class);
                intent.putExtra("list", subject);
                intent.putExtra("tuid", teacherUid);
                intent.putExtra("id", ids.get(i));
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        myholder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String subject = new Gson().toJson(subjects);
                Intent intent = new Intent(context, Add_sub.class);
                //  intent.putExtra("list", subject);
                intent.putExtra("subjects", sub_list.get(i));
                intent.putExtra("id", ids.get(i));
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return sub_list.size();
    }

    void addData(Subjects subjects, String id) {
        this.sub_list.add(subjects);
        this.ids.add(id);
        notifyDataSetChanged();
    }

    void removeData(int subjects) {
        this.sub_list.remove(subjects);
        this.ids.remove(subjects);
        notifyDataSetChanged();
    }

    private void getdata() {

        String uid = auth.getUid();
        db.collection("Subjects").document(uid).collection("subjects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Subjects subjects = document.toObject(Subjects.class);
                                Log.e("subjectCode ", subjects.sub_name);
                            }
                        } else {
                            Log.e("he", "Error getting documents.", task.getException());
                        }
                    }
                });

    }


    public class Myholder extends RecyclerView.ViewHolder {
        TextView tvsub_name, tvsub_type, tvsem, tv_dept, tv_code;
        ImageView check, chart, edit, person, delete;
        CardView cardView;

        public Myholder(@NonNull View itemView) {
            super(itemView);

            tvsub_name = itemView.findViewById(R.id.tv_subname);
            tvsub_type = itemView.findViewById(R.id.tvsub_type);
            tv_code = itemView.findViewById(R.id.tv_code);
            tvsem = itemView.findViewById(R.id.tv_sem);
            tv_dept = itemView.findViewById(R.id.tv_dept);
            check = itemView.findViewById(R.id.icheck);
            chart = itemView.findViewById(R.id.ichart);
            edit = itemView.findViewById(R.id.iedit);
            person = itemView.findViewById(R.id.iperson);
            delete = itemView.findViewById(R.id.idelete);
            cardView = itemView.findViewById(R.id.card);

        }

    }

    interface doAlert {
        void onAlert(int i, String s);
    }
}
