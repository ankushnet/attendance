package com.example.attendancee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    EditText editmail, editpass;
    ImageView btnlogin;
    TextView btnregister;
    FirebaseAuth auth;
    TextView fpass;
    Button teachback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fpass = findViewById(R.id.fpass);
        editmail = findViewById(R.id.editmail);
        editpass = findViewById(R.id.editpass);
        btnlogin = findViewById(R.id.loginbtn);
        btnregister = findViewById(R.id.registerbtn);
        teachback=findViewById(R.id.teachback);
        teachback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Hom_activity.class));
            }
        });
        auth = FirebaseAuth.getInstance();
        fpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, fpassword.class));
                finish();
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, register.class));
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newlogin();
            }
        });
    }

    private void newlogin() {
        String email = editmail.getText().toString().trim();
        String password = editpass.getText().toString().trim();
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editmail.setError("Enter valid email");
            return;

        }
        if (password.isEmpty() || password.length() < 6) {
            editpass.setError("Enter password");
            return;
        }
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    startActivity(new Intent(MainActivity.this, HomePage.class));
                } else {
                    util.toast(MainActivity.this, "Enter valid details");
                }
            }
        });
    }
}
