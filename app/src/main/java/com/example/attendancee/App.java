package com.example.attendancee;

import android.app.Application;
import android.content.SharedPreferences;

public class App extends Application {
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences=getSharedPreferences(getPackageName(),MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }
}
