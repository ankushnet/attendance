package com.example.attendancee;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.attendancee.R;
import com.example.attendancee.ViewAttendence;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ViewStuAdapter extends RecyclerView.Adapter<ViewStuAdapter.Myholder> {
    List<User> listt = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    Context context;


//    public ViewStuAdapter(ShowstuAttend showstuAttend) {
//        this.context = showstuAttend;
//    }

    public ViewStuAdapter(Studentnavigate context) {
        this.context=context;
    }

    @NonNull
    @Override
    public ViewStuAdapter.Myholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.viewstuatten, viewGroup, false);
        return new Myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewStuAdapter.Myholder myholder, int i) {
        final User user = listt.get(i);
        myholder.txtName.setText(user.getName());
        myholder.txtdepartment.setText(user.getDept());
        myholder.txtsem.setText(user.getPhone());

        myholder.txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,SubjectsActivity.class);
                intent.putExtra("uid", user.getUid());
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return listt.size();
    }

    public void addData(User teacherstatus, String id) {
        this.listt.add(teacherstatus);
        this.ids.add(id);
    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView txtName,
                txtdepartment, txtsem, txtview;

        public Myholder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.tv_Name);
            txtdepartment = itemView.findViewById(R.id.tv_Dept);
            txtsem = itemView.findViewById(R.id.tv_phn);
            txtview = itemView.findViewById(R.id.view_attendance);

        }
    }

}
