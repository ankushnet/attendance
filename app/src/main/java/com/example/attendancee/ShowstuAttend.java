package com.example.attendancee;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ShowstuAttend extends AppCompatActivity  {
    RecyclerView recyclerView;
    ViewStuAdapter viewStuAdapter;
    FirebaseAuth auth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showstu_attend);
        recyclerView = findViewById(R.id.recycler_view);
        auth=FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(viewStuAdapter);
        getsaveddata();
    }
    private void getsaveddata() {

        db.collection("users").document(auth.getUid())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful())
                {
                    User da=task.getResult().toObject(User.class);
                    db.collection("users")
                            .whereEqualTo("dept", da.getDept())
                            .whereEqualTo("type","Teacher")
                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful())
                            {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    User teacherstatus = document.toObject(User.class);
                                    viewStuAdapter.addData(teacherstatus, document.getId());
                                    viewStuAdapter.notifyDataSetChanged();
                                    //Log.e("subject ", subjects.sub_name);

                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ShowstuAttend.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

}
