package com.example.attendancee;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Subjects implements Parcelable {
    String sem, sub_name, sub_dept, sub_code, type;
    int rollfrom, rollto;

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public static Creator<Subjects> getCREATOR() {
        return CREATOR;
    }

    String current;


    protected Subjects(Parcel in) {
        sem = in.readString();
        sub_name = in.readString();
        sub_dept = in.readString();
        sub_code = in.readString();
        type = in.readString();
        rollfrom = in.readInt();
        rollto = in.readInt();
        current=in.readString();
    }

    public static final Creator<Subjects> CREATOR = new Creator<Subjects>() {
        @Override
        public Subjects createFromParcel(Parcel in) {
            return new Subjects(in);
        }

        @Override
        public Subjects[] newArray(int size) {
            return new Subjects[size];
        }
    };

    public int getRollfrom() {
        return rollfrom;
    }

    public void setRollfrom(int rollfrom) {
        this.rollfrom = rollfrom;
    }

    public int getRollto() {
        return rollto;
    }

    public void setRollto(int rollto) {
        this.rollto = rollto;
    }

    List<Student> studentList;

//    public List<Student> getList() {
//        return list;
//    }
//
//    public void setList(List<Student> list) {
//        this.list = list;
//    }

//    List<Student> list;

//    public Subjects(List<Student> list) {
//        this.list = list;
//    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Subjects(String sem, String sub_name, String sub_dept, String sub_code, String type, List<Student> studentList, int rollfrom, int rollto,String current) {
        this.sem = sem;
        this.sub_name = sub_name;
        this.sub_dept = sub_dept;
        this.sub_code = sub_code;
        this.type = type;
        this.studentList = studentList;
        this.rollfrom = rollfrom;
        this.rollto = rollto;
        this.current=current;
    }

    public Subjects() {

    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getSub_dept() {
        return sub_dept;
    }

    public void setSub_dept(String sub_dept) {
        this.sub_dept = sub_dept;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sem);
        dest.writeString(sub_name);
        dest.writeString(sub_dept);
        dest.writeString(sub_code);
        dest.writeString(type);
        dest.writeInt(rollfrom);
        dest.writeInt(rollto);
        dest.writeString(current);

    }
}
