package com.example.attendancee;

public class Student {
    //    private boolean isChecked;
//
//    public boolean getChecked() {
//        return isChecked;
//    }
//
//    public void setChecked(boolean checked) {
//        isChecked = checked;
//    }
    public Student(String name, int c, boolean b,String date) {
        this.name = name;
        this.roll_no = c;
        this.status = b;
        this.date=date;
    }

    public Student(String name, int c) {
        this.name = name;
        this.roll_no = c;

    }

    public Student() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(int roll_no) {
        this.roll_no = roll_no;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    String name;
    int roll_no;
    Boolean status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String date;
}
