package com.example.attendancee;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Studentfulldetails extends AppCompatActivity {
    int m_int = 1;

    TextView sem_numberr;
    TextView datee;
    EditText sub_names, sub_codes, sub_depts;
    RadioGroup radioGroup;
    RadioButton lectures, labs;
    String types = "";
    FirebaseAuth auth;
    FirebaseFirestore db;
    EditText rollno;
    Button btnsave;
    ProgressDialog progressDialog;
    final Long timestemp = new Date().getTime();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentfulldetails);
        sem_numberr = findViewById(R.id.sem_number_stu);
        datee = findViewById(R.id.datesched_stu);
        datee.setText(getDate(timestemp));
        sub_names = findViewById(R.id.sub_name_stu);
        sub_codes = findViewById(R.id.sub_code_stu);
        sub_depts = findViewById(R.id.sub_dept_stu);
        lectures = findViewById(R.id.rbtn_lecture_stu);
        labs = findViewById(R.id.rbtn_lab_stu);
        rollno = findViewById(R.id.roll_stu);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("wait a sec...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        radioGroup = findViewById(R.id.radiogroup_stu);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbtn_lecture_stu) {
                    types = lectures.getText().toString().trim();

                }
                if (checkedId == R.id.rbtn_lab_stu) {
                    types = labs.getText().toString().trim();
                }

            }
        });
        btnsave = findViewById(R.id.button_save);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String semnumber = sem_numberr.getText().toString().trim();
                String subname = sub_names.getText().toString().trim();
                String subcode = sub_codes.getText().toString().trim();
                String subdept = sub_depts.getText().toString().trim();
//                String date = datee.getText().toString().trim();
                int rollnumber = Integer.parseInt(rollno.getText().toString().trim());
                if (TextUtils.isEmpty(semnumber)) {
                    sem_numberr.setError("Select semester");
                    return;
                }
                if (TextUtils.isEmpty(subname)) {
                    sub_names.setError("Enter subject name");
                    return;
                }
                if (TextUtils.isEmpty(subcode)) {
                    sub_codes.setError("Enter subject code");
                    return;
                }
                if (TextUtils.isEmpty(subdept)) {
                    sub_depts.setError("Enter subject dept");
                    return;
                }
                if (TextUtils.isEmpty(String.valueOf(rollnumber))) {
                    rollno.setError("Enter roll no.");
                    return;
                }
                StudentBean studentBean = new StudentBean(semnumber, subname, subdept, subcode, types, rollnumber);
                storestudata(studentBean);
            }
        });


    }

    private void storestudata(StudentBean studentBean) {
        progressDialog.show();

        db.collection("Student").document(FirebaseAuth.getInstance().getUid()).set(studentBean).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                util.toast(Studentfulldetails.this, "Subject added");
                startActivity(new Intent(Studentfulldetails.this, Studentnavigate.class));

            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.hide();
                Log.w("hey", "Error");
            }
        }).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    progressDialog.dismiss();
            }
        });
    }

    private String getDate(long time_stamp_server) {
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy");
        return formatter.format(time_stamp_server);
    }

    public void DecreaseInt(View view) {

        if (m_int > 1)
            m_int = m_int - 1;
        else {
            System.out.print("Semester starts from one");
        }
        display(m_int);

    }

    public void IncreaseInt(View view) {


        m_int = m_int + 1;
        display(m_int);
    }

    private void display(int number) {
        TextView textView = findViewById(R.id.sem_number_stu);
        textView.setText("" + number);


    }
}
