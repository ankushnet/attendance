package com.example.attendancee;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class AttenAdapter extends RecyclerView.Adapter<AttenAdapter.Myhold> {


    boolean isUpdated = true;
    List<Student> list;
    Context context;
    HashMap<Integer, Boolean> map;
    boolean flag = false;
    ImageView btnselectall, unselectall;
//    private SparseBooleanArray itemStateArray;

    public AttenAdapter(Context context, List<Student> subject, ImageView btnselectall, ImageView unselectall) {
        this.context = context;
        list = subject;
        this.btnselectall = btnselectall;
        this.unselectall = unselectall;
        map = new HashMap<>();
    }

    @NonNull
    @Override
    public AttenAdapter.Myhold onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.take_atten, viewGroup, false);
        return new Myhold(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AttenAdapter.Myhold myhold, final int i) {

        Student student = list.get(i);
//        myhold.checkBox.setVisibility(student.showCheckbox ? View.VISIBLE : View.GONE);
        map.put(student.roll_no, false);
        myhold.roll.setText(String.valueOf(student.roll_no));
        myhold.name.setText(student.name);
        myhold.checkBox.setChecked(student.status);
        btnselectall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (flag == false) {
                    showAllBoxes();
                    Toast.makeText(context, "Marked All as Present", Toast.LENGTH_SHORT).show();
                    flag = true;

                }

                unselectall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (flag == true) {
                            hideAllBoxes();
                            Toast.makeText(context, "Marked All as Absent", Toast.LENGTH_SHORT).show();

                            flag = false;

                        }

                    }
                });


            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Myhold extends RecyclerView.ViewHolder {
        TextView roll, name;
        CheckBox checkBox;

//        Button btnselect;

        public Myhold(@NonNull View itemView) {
            super(itemView);
            this.setIsRecyclable(true);
            roll = itemView.findViewById(R.id.txtrollc);
//            btnselect=itemView.findViewById(R.id.btnselect);
            name = itemView.findViewById(R.id.txtnamec);
            checkBox = itemView.findViewById(R.id.checkb);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    list.get(getAdapterPosition()).setStatus(isChecked);

                }
            });
        }

    }

    private void showAllBoxes() {

        for (Student student : list) {
            student.setStatus(true);
        }
        notifyDataSetChanged();
    }

    private void hideAllBoxes() {

        for (Student student : list) {
            student.setStatus(false);
        }
        notifyDataSetChanged();
    }
}
