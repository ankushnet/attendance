package com.example.attendancee;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {
    TextView ptvname, ptvmail, ptvphone, ptvdept;
    FirebaseFirestore db;
    FirebaseAuth auth;
    ImageView studentimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ptvname = findViewById(R.id.ptvname);
        ptvmail = findViewById(R.id.ptvmail);
        ptvphone = findViewById(R.id.ptvphone);
        ptvdept = findViewById(R.id.ptvdept);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        studentimg = findViewById(R.id.studentimg);
        db.collection("users").document(auth.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    User user = task.getResult().toObject(User.class);
                    ptvname.setText(user.getName());
                    ptvmail.setText(user.getMail());
                    ptvphone.setText(user.getPhone());
                    ptvdept.setText(user.getDept());
                    Picasso.get().load(user.getProfileuri()).into(studentimg);

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
