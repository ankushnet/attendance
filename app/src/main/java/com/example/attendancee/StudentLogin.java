package com.example.attendancee;

import android.content.Intent;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import static com.example.attendancee.Preferences.saveUser;

public class StudentLogin extends AppCompatActivity {
    EditText editmail, editpass;
    ImageView btnlogin;
    TextView btnreg;
    Button backbutton;
    TextView fpassword;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_login2);
        auth = FirebaseAuth.getInstance();
        editmail = findViewById(R.id.editmail_stu);
        backbutton=findViewById(R.id.bthome);
        editpass = findViewById(R.id.editpass_stu);
        btnlogin = findViewById(R.id.loginbtn_stu);
        btnreg = findViewById(R.id.registerbtn_stu);
        fpassword=findViewById(R.id.fpass_stu);
        fpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), com.example.attendancee.fpassword.class));
            }
        });
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Hom_activity.class));
            }
        });
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentLogin.this, Studentregister.class));
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stulogin();
            }
        });
    }

    private void stulogin() {
        String email = editmail.getText().toString().trim();
        String password = editpass.getText().toString().trim();
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editmail.setError("Enter valid mail");
            return;
        }
        if (password.isEmpty() || password.length() < 6) {
            editpass.setError("Enter password");
            return;
        }
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseFirestore.getInstance().collection("Student")
                            .document(task.getResult().getUser().getUid())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful())
                                    {
                                        User user=task.getResult().toObject(User.class);
                                       saveUser(user);
                                        startActivity(new Intent(StudentLogin.this,Studentnavigate.class));
                                        finish();
                                    }
                                }
                            });
                     }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(StudentLogin.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
