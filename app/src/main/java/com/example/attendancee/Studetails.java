package com.example.attendancee;


import java.util.Date;

class Studetails {

    public Studetails(String s, String name, String mail, String phone, String password, String dept) {
        this.name = name;
        this.mail = mail;
        this.phone = phone;
        this.password = password;
        this.timestemp= new Date().getTime();
        this.dept = dept;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    String name;
    String mail;
    String phone;
    String password;

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    String dept;

    public Long getTimestemp() {
        return timestemp;
    }

    public void setTimestemp(Long timestemp) {
        this.timestemp = timestemp;
    }

    Long timestemp=0L;
}

